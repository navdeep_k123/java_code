import java.util.Arrays;
class Selectionsort
{
public static int[] Selectionsort(int arr[])
{
for(int i=0; i<arr.length-1; i++)
{
int index=i;

for(int j=i+1; j<arr.length; j++)

if(arr[j]<arr[index])
index=j;
int smallNumber=arr[index];
arr[index]=arr[i];
arr[i]=smallNumber;
}
return arr;
}

public static void main(String[]args)
{
int arr[]={40,10,-30,45,39,32};
System.out.println("before sorting: ");
System.out.println(Arrays.toString(arr));
arr=Selectionsort(arr);
System.out.print("============");
System.out.print("after sorting: ");
System.out.print(Arrays.toString(arr));
}
}

