import java.util.Arrays;
class Shellsort
{
public static void main(String[]args)
{
int[]array={22,53,33,12,75,65,125};
System.out.println("before sorting: ");
System.out.println(Arrays.toString(array));
System.out.print("============");
System.out.print("after sorting: ");
array =Shellsort(array);
System.out.print(Arrays.toString(array));
}
public static int[] Shellsort(int[]array)
{
int h=1;
while(h<=array.length/3)
{
h=3*h+1;
}
while(h>0)
{
for(int i=0; i<array.length; i++)
{
int temp= array[i];
int j;
for(j=i; j>h-1 && array[j-h]>=temp; j=j-h)
{
array[j]=array[j-h];
}
array[j]=temp;
}
h=(h-1)/3;
}
return array;
}
}
